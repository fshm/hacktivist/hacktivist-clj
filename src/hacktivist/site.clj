(ns hacktivist.site
    (:require
     [ring.util.response :as ring-resp]
     [io.pedestal.http.csrf :as csrf])
  (:use
    [hiccup.page :only (html5 include-css include-js)]))
 
(defn base [title & content]
  (html5 {:lang "en"}
         [:head
          [:meta {:charset "utf-8"}]
          [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
          [:link {:rel "icon" :type "image/png" :href "/img/favicon.png"}]
          [:title title]
          (include-css "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.1/css/bulma.min.css")]
         content))

(defn form-control-input
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "text" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-textarea
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:textarea.textarea {:type "text" :placeholder text :name param-name :value value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-email
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "email" :placeholder text :name param-name :value value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-date
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control
                                   [:input.input {:type "date" :name param-name :value value}]]
   [:p.help "Click to choose a date"]])

(defn form-control-password
  "Password form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "password" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-checkbox
  "Checkbox form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.checkbox text] [:div.control [:input {:type "checkbox" :placeholder text :name param-name :value value}]]])

(defn form-hidden-value
  "Default form control for dynamic deployment"
  [param-name & value]
  [:input {:type "hidden" :name param-name :value value}])

(defn form-anti-forgery-token
  "CSRF anti-forgery-token"
  [request]
  (form-hidden-value "__anti-forgery-token" (csrf/anti-forgery-token request)))

(defn form-control-dropdown
  "Dropdown form control for dynamic deployment. Accepts label name, form value name and value pairs for each option."
  [text name option-pairs]

  [:div.field [:label.label text]
   [:div.control
    [:div.select.is-primary
     [:select {:name name}
      (for [center option-pairs]
        [:option {:value (second center)} (first center)])]]]])

;;      (for [x options y values]
;;       [:option {:value (str y)} (str x)])]]]])


(defn form-control-button
  "Button control for a form"
  [name]
  [:div.control [:button.button.is-primary.is-medium name]])

(defn notification-success
  "Notification control"
  [message]
  [:div.notification.is-primary message
   [:button.delete]])

(defn notification-failure
  "Notification control"
  [message]
  [:div.notification.is-danger message
   [:button.delete]])
