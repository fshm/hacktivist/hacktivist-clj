(ns hacktivist.auth.users.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "hacktivist/auth/users/users.sql")
