(ns hacktivist.auth.users.forms
    (:require [hacktivist.site :as site]))

(defn register-control [request]
  [:div.column.is-one-third.is-offset-one-third
   [:h1.title.has-text-centered "Register"]
   [:form {:method "POST" :action "/register"}
    (site/form-anti-forgery-token request)
    (site/form-control-input "Username" "username")
    (site/form-control-email "Email" "email")
    (site/form-control-password "Password" "password")
    (site/form-control-password "Confirm password" "confirm-password")
    (site/form-control-button "Submit")]])

(defn login-control [request]
  [:div.column.is-one-third.is-offset-one-third
   [:h1.title.has-text-centered "Login"]
   [:form {:method "POST" :action "/login"}
    (site/form-anti-forgery-token request)
    (site/form-control-input "Username" "username")
    (site/form-control-password "Password" "password")
    (site/form-control-button "Submit")]])
