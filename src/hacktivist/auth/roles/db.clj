(ns hacktivist.auth.roles.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "hacktivist/auth/roles/roles.sql")
