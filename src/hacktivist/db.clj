(ns hacktivist.db
    (:require [clojure.edn :as edn]
              [hikari-cp.core :as hikari]
;;            [clojure.java.jdbc :as jdbc]
            [ragtime.jdbc :as ragtime]))


(def datasource-options (if (= (System/getenv "TESTING") "true")
                          (assoc (clojure.edn/read-string (slurp "resources/env/jdbc.edn")) :server-name "postgres")
                          (clojure.edn/read-string (slurp "resources/env/jdbc.edn"))))

(defonce datasource
  (delay (hikari/make-datasource datasource-options)))

(def jdbc {:datasource @datasource})

(def config
  {:datastore  (ragtime/sql-database jdbc)
   :migrations (ragtime/load-resources "migrations")})
