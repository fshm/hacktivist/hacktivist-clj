(ns hacktivist.auth
    (:require
     [io.pedestal.interceptor :refer [interceptor]]
     [io.pedestal.interceptor.error :refer [error-dispatch]]
     [buddy.hashers :as hashers]
     [buddy.auth :as auth]
     [hacktivist.auth.users.db :as users]
     [hacktivist.db :as db]))

(defn hash-password
  "Wrapper to hash password to abstract algorithm"
  [password]
  (hashers/derive password {:alg :bcrypt+blake2b-512}))

(defn authenticate-username-password
  "Authenticates the username & password. First checks if the user exists"
  [data]
  (let [user (users/user-by-username db/jdbc data)]
    (if (nil? user)
      nil
      (let [attempt (:password data)
            password (:password user)]
        (if (hashers/check attempt password)
          user
          nil)))))

(defn authenticate-email-password
  "Authenticates the email & password. First checks if the user exists"
  [data]
  (let [user (users/user-by-email db/jdbc data)]
    (if (nil? user)
      nil
      (let [attempt (:password data)
            password (:password user)]
        (if (hashers/check attempt password)
          user
          nil)))))

(defn throw-forbidden
  "Throws the access forbbidden exception with [info] content.
Default error type :unauthenticated."
  ([] (throw-forbidden {}))
  ([info]
   (throw (ex-info "403 Forbidden" (merge {:exception-type :roles-unauthorized} info)))))

(defn superuser?
  "Checks if the request has superuser privileges"
  [request]
  (-> request :session :identity :is_superuser))

(defn authorized?
  "Checks if an authenticated request has required roles."
  [request required-roles]
  (let [granted-roles (get-in request [:session :identity :roles])]
    (not (empty? (clojure.set/intersection (set granted-roles) required-roles)))))

(defn- guard-with
 ""
  [roles]
  (fn [{request :request :as context}]
    (if (auth/authenticated? request)
      (if-not (or (empty? roles)
                  (superuser? request)
                  (authorized? request roles))
        (throw-forbidden)
;;(io.pedestal.interceptor.helpers/handler unauthorized)
        context)
      (auth/throw-unauthorized))))

(defn guard
  "An interceptor that allows only authenticated users that have any of :roles to access unterlying pages.
Accepts optional parameters:
:roles              - a set of roles that are allowed to access the page, if not defined users are required to be just authenticated"
  ([] (guard #{}))
  ([roles]
   (interceptor {:name  ::guard
                 :enter (guard-with roles)})))
