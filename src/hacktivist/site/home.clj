(ns hacktivist.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [hacktivist.site :as site]
            ))

(defn home-page [request]
  (ring-resp/response "Hello World!"))
