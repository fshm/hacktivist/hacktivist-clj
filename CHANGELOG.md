# hacktivist CHANGELOG

Refer https://keepachangelog.com/en/1.0.0/ for format

## [0.1.0] - 2022-01-09

### Added

- Added AGPLv3 LICENSE
- Initialized code from column
- Added migrations from the sfd-app
