(defproject hacktivist "0.2.0-SNAPSHOT"
  :description "Social network for those who want to change the world "
  :url "https://www.fshm.org"
  :license {:name "AGPLv3 with classpath exceptions for EPL licensed software"
            :url "https://www.gnu.org/licenses/agpl.txt"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [io.pedestal/pedestal.service "0.5.9"]
                 [io.pedestal/pedestal.service-tools "0.5.9"
                   :exclusions [[ring/ring-core]]]
                 ;; Remove this line and uncomment one of the next lines to
                 ;; use Immutant or Tomcat instead of Jetty:
                 [io.pedestal/pedestal.jetty "0.5.9"
                  :exclusions [[org.eclipse.jetty/jetty-server]
                               [org.eclipse.jetty.websocket/websocket-api]
                               [org.eclipse.jetty.websocket/websocket-server]
                               [org.eclipse.jetty/jetty-alpn-server]
                               [org.eclipse.jetty/jetty-servlet]
                               [org.eclipse.jetty.http2/http2-server]
                               [org.eclipse.jetty.websocket/websocket-servlet]]]
                 ;;[io.pedestal/pedestal.immutant "0.5.9"]
                 ;;[io.pedestal/pedestal.tomcat "0.5.9"]
                 [ch.qos.logback/logback-classic "1.2.10"
                  :exclusions [org.slf4j/slf4j-api]]
                 [org.slf4j/jul-to-slf4j "1.7.32"]
                 [org.slf4j/jcl-over-slf4j "1.7.32"]
                 [org.slf4j/log4j-over-slf4j "1.7.32"]
                 [hiccup "2.0.0-alpha2"]
                 ;; [org.clojure/clojurescript "1.10.866"]
                 [hikari-cp "2.13.0"]
                 [ragtime "0.8.1"]
                 [org.postgresql/postgresql "42.3.1"]
                 [com.layerware/hugsql "0.5.1"]
                 [buddy/buddy-auth "3.0.1"]
                 [buddy/buddy-hashers "1.8.1"]
                 [cheshire "5.10.1"]
                 [org.eclipse.jetty.websocket/websocket-api "9.4.44.v20210927"]
                 [org.eclipse.jetty.websocket/websocket-server "9.4.44.v20210927"]
                 [org.eclipse.jetty/jetty-alpn-server "9.4.44.v20210927"]
                 [org.eclipse.jetty/jetty-servlet "9.4.44.v20210927"]
                 [org.eclipse.jetty.http2/http2-server "9.4.44.v20210927"]
                 [org.eclipse.jetty.websocket/websocket-servlet "9.4.44.v20210927"]
                 [ring/ring-core "1.9.4"]
                 [joda-time "2.10.13"]]
  :min-lein-version "2.0.0"
  :resource-paths ["config", "resources"]
  ;; If you use HTTP/2 or ALPN, use the java-agent to pull in the correct alpn-boot dependency
  ;:java-agents [[org.mortbay.jetty.alpn/jetty-alpn-agent "2.0.5"]]
  :profiles {:dev {:aliases {"run-dev" ["trampoline" "run" "-m" "hacktivist.server/run-dev"]
                             "migrate"  ["run" "-m" "hacktivist.server/migrate"]
                             "rollback" ["run" "-m" "hacktivist.server/rollback"]}
                   :dependencies [[io.pedestal/pedestal.service-tools "0.5.9"]]}
             :uberjar {:jar-exclusions [#"/media/",#"/logs/"]
                       :uberjar-exclusions [#"/media/",#"/logs/"]
                       :aot [hacktivist.server]}}
  :main ^{:skip-aot true} hacktivist.server)
